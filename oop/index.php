<?php
//require('animal.php');
require('ape.php');
require('frog.php');

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "legs : " . $sheep->legs . "<br>"; // 4
echo "cold blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

$kodok = new frog("buduk");

echo "Name : " . $kodok->name . "<br>"; 
echo "legs : " . $kodok->legs . "<br>"; 
echo "cold blooded : " . $kodok->cold_blooded . "<br>"; 
echo "Jump : " . $kodok->jump() . "<br><br>";

$sungokong = new ape("kera sakti");

echo "Name : " . $sungokong->name . "<br>"; 
echo "legs : " . $sungokong->legs . "<br>"; 
echo "cold blooded : " . $sungokong->cold_blooded . "<br>"; 
echo "Yell : " . $sungokong->yell();





?>