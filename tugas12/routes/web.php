<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/table', function(){
    return view('halaman.table');
});

Route::get('/data-table', function(){
    return view('halaman.datatable');
});

//CRUD section
//create form tambah cast
route::get('/cast/create', [CastController::class, 'create']);
//create menyimpan data cast
route::post('/cast', [CastController::class, 'store']);

//read cast
//menampilkan semua data
route::get('/cast', [CastController::class, 'index']);
//tampil data berdasat id
route::get('/cast/{cast_id}', [CastController::class, 'show']);

//route menuju form update table
route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

//route untuk update data
route::put('/cast/{cast_id}', [CastController::class, 'update']);


//route destroy
route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);