@extends('Layouts.master')
@section('judul')
Halaman bio Cast
@endsection
@section('subjudul')
bio Cast
@endsection
@section('content')
<h1>{{$cast->nama}}</h1>
<p>{{$cast->bio}}</p>

<a href="/cast" class = "btn btn-secondary btn-sm">kembali</a>
@endsection