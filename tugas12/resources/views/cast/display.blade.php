@extends('Layouts.master')
@section('judul')
Halaman Table Cast
@endsection
@section('subjudul')
Table Cast
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">Nomor</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>

                <form action="/cast/{{$item->id}}" method="POST">
                    @method('delete')
                    @csrf
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">bio</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
 
                </form>
            </td>
        </tr>
            
        @empty
        <tr>
            <td>Data Kosong</td>
        </tr>
            
        @endforelse

    </tbody>
  </table>
@endsection