@extends('Layouts.master')
@section('judul')
Halaman Tambah Cast
@endsection
@section('subjudul')
Tambah Cast
@endsection
@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label >nama</label>
        <input type="text" class="form-control" name="nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label >umur</label>
        <input type="text" class="form-control" name="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label >bio</label>
        <textarea name="bio" class="form-control"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection