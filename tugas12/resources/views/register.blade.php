@extends('Layouts.master')
@section('judul')
Register
@endsection
@section('subjudul')
Register
@endsection
@section('content')
<h2>Buat Account Baru!</h2>
<h4>Sign Up Form</h4>
<form action="/welcome" method="post">
    @csrf
    <label>First name:</label><br><br>
    <input type="text" name="fname"><br><br>
    <label>Last name:</label><br><br>
    <input type="text" name="lname"><br><br>
    <label>Gender:</label> <br><br>
    <input type="radio" name="gender"> Male <br>
    <input type="radio" name="gender"> Female <br>
    <input type="radio" name="gender"> Other <br><br>
    <label>Nationality:</label><br><br>
    <select name="Nationality">
        <option value="">Indonesian</option>
        <option value="">Malaysia</option>
        <option value="">Japan</option>
        <option value="">Saudi Arabia</option>
    </select> <br> <br>
    <label>Language Spoken:</label> <br><br>
    <input type="checkbox" name="Language Spoken">Bahasa Indonesia <br>
    <input type="checkbox" name="Language Spoken">English <br>
    <input type="checkbox" name="Language Spoken">Other <br><br>
    <label>Bio:</label><br><br>
    <textarea cols="30" rows="10"></textarea></textarea> <br>

    <input type="submit" value="Sign up">



</form>
@endsection
 
    
