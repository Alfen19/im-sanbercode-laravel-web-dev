<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        $namadepan = $request['fname'];
        $namabelakang = $request['lname'];

        return view('welcome', ['namadepan'=>$namadepan, 'namabelakang'=>$namabelakang]);
    }
}
